package controle.validadores;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public interface Senha implements Validacao {
	

Pattern padraoSenha = Pattern.compile("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})");

/*
(					# Start of group
(?=.*\d)			#   must contains one digit from 0-9
(?=.*[a-z])			#   must contains one lowercase characters
(?=.*[A-Z])			#   must contains one uppercase characters
(?=.*[@#$%])		#   must contains one special symbols in the list "@#$%"
  .					#   match anything with previous condition checking
{6,20}				#   length at least 6 characters and maximum of 20
)					# End of group*/			


	
	@Override
	public boolean executarSenha(Seguranca seguranca ) { 
		
		if(seguranca.getSenha().isEmpty()) {
		System.out.println("O campo n�o pode estar vazio");
		}
		else {
	
			Matcher m = padraoSenha.matcher(seguranca.getSenha());
			if(m.matches()){
				return true;
			}
			else{
				return false;
			}
		}
	}

}
}