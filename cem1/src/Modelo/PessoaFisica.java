package Modelo;

import java.util.Date;

public abstract class PessoaFisica extends Pessoa 
{
	private Date datanascimento;
	private String cpf;
	private String rg;
	private String tiposanguineo[] = {"A+", "A-", "B+", "B-", "AB+", "AB-", "O+", "O-"};
	private String sis;
	private String sus;
	private String foto;
	private String biometria;
	private String profissao;
	private String escolaridade;
	private String estadocivil[] = {"solteiro(a)", "casado(a)", "vi�vo(a)", "divorciado(a)", "uni�o est�vel"};
	private String nomedamae;
	private String nomedopai;
	
	
	// atestado medico
	// frequentador ou aluno
	// pj?	
	
	
	public Date getDatanascimento() {
		return datanascimento;
	}
	public void setDatanascimento(Date datanascimento) {
		this.datanascimento = datanascimento;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public String[] getTiposanguineo() {
		return tiposanguineo;
	}
	public void setTiposanguineo(String[] tiposanguineo) {
		this.tiposanguineo = tiposanguineo;
	}
	public String getSis() {
		return sis;
	}
	public void setSis(String sis) {
		this.sis = sis;
	}
	public String getSus() {
		return sus;
	}
	public void setSus(String sus) {
		this.sus = sus;
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	public String getBiometria() {
		return biometria;
	}
	public void setBiometria(String biometria) {
		this.biometria = biometria;
	}
	public String getProfissao() {
		return profissao;
	}
	public void setProfissao(String profissao) {
		this.profissao = profissao;
	}
	public String getEscolaridade() {
		return escolaridade;
	}
	public void setEscolaridade(String escolaridade) {
		this.escolaridade = escolaridade;
	}
	public String[] getEstadocivil() {
		return estadocivil;
	}
	public void setEstadocivil(String[] estadocivil) {
		this.estadocivil = estadocivil;
	}
	public String getNomedamae() {
		return nomedamae;
	}
	public void setNomedamae(String nomedamae) {
		this.nomedamae = nomedamae;
	}
	public String getNomedopai() {
		return nomedopai;
	}
	public void setNomedopai(String nomedopai) {
		this.nomedopai = nomedopai;
	}
}
