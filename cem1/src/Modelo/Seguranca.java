package Modelo;

public class Seguranca {

	private AlunoDependente aluno;
	private Beneficiario beneficiario;
	private String login;
	private String senha;
	private String fraseseguranca;
	private String chaverecuperacao;
	
	
	public AlunoDependente getAluno() {
		return aluno;
	}
	public void setAluno(AlunoDependente aluno) {
		this.aluno = aluno;
	}
	public Beneficiario getBeneficiario() {
		return beneficiario;
	}
	public void setBeneficiario(Beneficiario beneficiario) {
		this.beneficiario = beneficiario;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getFraseseguranca() {
		return fraseseguranca;
	}
	public void setFraseseguranca(String fraseseguranca) {
		this.fraseseguranca = fraseseguranca;
	}
	public String getChaverecuperacao() {
		return chaverecuperacao;
	}
	public void setChaverecuperacao(String chaverecuperacao) {
		this.chaverecuperacao = chaverecuperacao;
	}
}
