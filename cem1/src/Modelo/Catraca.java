package Modelo;

public class Catraca {

	
	private String marca;
	private String modelo;
	private String numeroserie;
	private String numeropatrimonio;
	
	
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getNumeroserie() {
		return numeroserie;
	}
	public void setNumeroserie(String numeroserie) {
		this.numeroserie = numeroserie;
	}
	public String getNumeropatrimonio() {
		return numeropatrimonio;
	}
	public void setNumeropatrimonio(String numeropatrimonio) {
		this.numeropatrimonio = numeropatrimonio;
	}

}
