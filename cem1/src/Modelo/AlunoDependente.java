package Modelo;

public class AlunoDependente extends Beneficiario {

	private PessoaFisica pessoafisicaresponsavel;

	
	// Getters and Setters
	public PessoaFisica getPessoafisicaresponsavel() {
		return pessoafisicaresponsavel;
	}

	public void setPessoafisicaresponsavel(PessoaFisica pessoafisicaresponsavel) {
		this.pessoafisicaresponsavel = pessoafisicaresponsavel;
	}

}
