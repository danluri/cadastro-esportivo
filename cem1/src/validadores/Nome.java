package controle.validadores;

import Modelo.Pessoa;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Nome implements Validacao {


	Pattern padraoNome = Pattern.compile("/(?=^.{2,60}$)^[A-ZÀÁÂĖÈÉÊÌÍÒÓÔÕÙÚÛÇ][a-zàáâãèéêìíóôõùúç]+(?:[ ](?:das?|dos?|de|e|[A-Z][a-z]+))*");

	
	@Override
	public boolean executarNome(Pessoa nome) { 
		
		if(nome.getNome().isEmpty()) {
		System.out.println("O campo não pode estar vazio");
		}
		else {
			
			Matcher m = padraoNome.matcher(nome.getNome());
			if(m.matches()){
				return true;
			}
			else{
				return false;
			}
		}
	}
}
