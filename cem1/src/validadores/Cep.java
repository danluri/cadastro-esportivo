package controle.validadores;
import Modelo.Endereco;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public interface Cep implements Validacao{
	
	
	
	Pattern padraoCep = Pattern.compile("^\\d{5}-\\d{3}$");
	// ou podemos usar o \\d{8} e desabilitar o "-" no front
	
	//Essa express�o indica que para uma String passar na valida��o(matches), 
	//ela precisa come�ar com 5 d�gitos seguido de um tra�o e terminar com mais 3 d�gitos.

	@Override
	public boolean executarCep(Endereco endereco) { 
		
		if(endereco.getCep().isEmpty()) {
		System.out.println("O campo n�o pode estar vazio");
		}
		else {
		
		Matcher m = padraoCep.matcher(endereco.getCep());
			if(m.matches()){
				
				
				return true;
			}
			else{
				return false;
			}
		}
}
}


